﻿namespace Experiments.InheritanceAndOverload
{
    public interface INonAnalytics
    {
        void Execute();
    }
}
