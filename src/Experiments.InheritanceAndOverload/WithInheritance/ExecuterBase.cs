﻿namespace Experiments.InheritanceAndOverload.WithInheritance
{
    public class ExecuterBase
    {
        public virtual void Execute(Analytics analytics)
        {
            analytics?.Execute();
        }
    }
}
