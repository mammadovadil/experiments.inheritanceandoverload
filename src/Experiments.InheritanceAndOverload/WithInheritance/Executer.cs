﻿namespace Experiments.InheritanceAndOverload.WithInheritance
{
    public class Executer : ExecuterBase
    {
        public void Experiment()
        {
            var analytics = new Analytics();
            // I don't use base.Execure(analytics) beacuse I need overrided method to be called
            Execute(analytics);
        }

        // I don't use (INonAnalytics nonAnalytics) instead of
        // generic type, because in original project I have more type
        // constraints and I need generic type parameter inside this method for use.
        public void Execute<TNonAnalytics>(TNonAnalytics nonAnalytics)
            where TNonAnalytics : INonAnalytics
        {
            nonAnalytics?.Execute();
        }

        public override void Execute(Analytics analytics)
        {
            // some additional staff goes here
            base.Execute(analytics);
        }
    }
}
