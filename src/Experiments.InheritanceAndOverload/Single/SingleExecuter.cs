﻿namespace Experiments.InheritanceAndOverload.Single
{
    public class SingleExecuter
    {
        public void Experiment()
        {
            var analytics = new Analytics();
            Execute(analytics);
        }

        public void Execute(Analytics analytics)
        {
            analytics?.Execute();
        }

        public void Execute<TNonAnalytics>(TNonAnalytics nonAnalytics)
            where TNonAnalytics : INonAnalytics
        {
            nonAnalytics?.Execute();
        }
    }
}
